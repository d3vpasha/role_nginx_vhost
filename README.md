Role Nginx Vhost
=========

This role installs and configures Nginx vhosts

Requirements
------------

This role was tested using Ansible v2.9.10.

This role was tested against Ubuntu server.

If your domain name is not yours and you use fake values, you should modify the file `/etc/hosts` and add a new line for each domain. Please find below an example :

	172.20.0.1 devops-hml.datadome.co www.devops-hml.datadome.co

You can see that the first value is your machine IP adress followed by your domain names.

Role Variables
--------------

| Variable          | Optional/Mandatory | Default | Description                                                                                    |
|-------------------|--------------------|---------|------------------------------------------------------------------------------------------------|
| nginx_vhost_state | O                  | present | Set to present or to absent if you would like to install or uninstall this role, respectively. |
| nginx_vhost_name  | M                  |         | The domain name                                                                                |
| nginx_vhost_port  | O/M                |         | Mandatory when `nginx_vhost_state` is set to `present`. The listening port                     |

Dependencies
------------

No dependencies.

Example Playbook
----------------

Install :

    - hosts: servers
      roles:
         - { role: role_nginx_vhost, nginx_vhost_name: "devops-hml.datadome.co", nginx_vhost_port: 84 }

Uninstall :

    - hosts: servers
      roles:
         - { role: role_nginx_vhost, nginx_vhost_state: 'absent', nginx_vhost_name: "devops-hml.datadome.co" }

License
-------

BSD

Author Information
------------------

Mehmet DEMIR (d3vpasha)
